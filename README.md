# Elixir Müşteri Takip - Elixir Customer Tracking
Müşteri Takip Programı, Laravel 5.1
Migrations ile kurulumunuzu yapabilirsiniz. 
Örnek veritabanı dosyasını SQL.sql olarak proje ana dizininde mevcuttur.
[![Latest Stable Version](https://poser.pugx.org/osmanyilmazco/customer/version)](https://packagist.org/packages/osmanyilmazco/customer) [![Latest Unstable Version](https://poser.pugx.org/osmanyilmazco/customer/v/unstable)](//packagist.org/packages/osmanyilmazco/customer) [![Total Downloads](https://poser.pugx.org/osmanyilmazco/dosyadepola/downloads)](https://packagist.org/osmanyilmazco/customer/phpunit)


# Laravel Hakkında
Laravel 5.1 kurulumunuz yapıp veritabanınız oluşturduktan sonra projeyi indirip dosyaları üzerine atıp kullanabilirsiniz.

# Kurulum
### Özet Anlatım:
**https://github.com/osmanyilmazco/customer/archive/master.zip** 
Dosyayı indirip localhost'a atıyoruz ve konsoldan ```composer install``` Komutunu yazdırıyoruz
## Veritabanı Ayarları
kök dizindeki **.env** dosyasının içine veritabanı bilgilerimizi yazıyoruz
**Örnek bağlantı**
```
DB_HOST=localhost
DB_DATABASE=laravel5_customer
DB_USERNAME=root
DB_PASSWORD=
```

### Geniş Anlatım:
**https://github.com/osmanyilmazco/customer/archive/master.zip**
Bağlantı adresinden dosyayı indiriyoruz ve apache (localhost) sunucumuza atıyoruz.
Şimdi dosyamızı indirmiş olduğumuz klasörünü içine giriyoruz, kurulumu yapılması için konsoldan (cmd/terminal) projenin klasörüne girip ```composer install``` Komutunu yazıyoruz

### En son olarak migrasyonların çalışması için
```
php artisan migrate
```
komutumuzu konsola yazdıktan sonra sistemi kullanmaya başlayabiliriz. :)

### Kurulum Görselleri:
####```Dosyamızı indirmiş olduğumuz klasörün içine giriyoruz```
![Customer](http://indir.astald.com/dosyalar/screenshot_1_db_56e2f50a3a506.png)
####```windows için (windows+r) tuşuna basıyoruz (ubuntu için terminale giriyoruz)```
![Customer](http://indir.astald.com/dosyalar/screenshot_2_db_56e2f50a3e93e.png)
####```konsoldan apache sunucunun kurulu olduğu klasöre giriyoruz```
![Customer](http://indir.astald.com/dosyalar/screenshot_3_db_56e2f50a429d9.png)
####```composer install komutunu yazıyoruz``` 
![Customer](http://indir.astald.com/dosyalar/screenshot_4_db_56e2f50a45a2e.png)
####```kurulum başlıyor``` 
![Customer](http://indir.astald.com/dosyalar/screenshot_6_db_56e2f50a4b6c4.png)
![Customer](http://indir.astald.com/dosyalar/screenshot_9_db_56e2f50a55e95.png)
####```kurulum başarıyla gerçekleşti``` 
![Customer](http://indir.astald.com/dosyalar/screenshot_7_db_56e2f50a4ed95.png)
####```veritabanı bağlantı ayarlarını yapmak için editörümüz ile açıyoruz``` 
![Customer](http://indir.astald.com/dosyalar/screenshot_8_db_56e2f50a51df5.png)

# Ubuntu (Linux) Kurulum
####```Dosyayı indirdikten sonra``` 
####```terminalı açıyoruz``` 
![Customer](http://indir.astald.com/dosyalar/screenshot-from-2016-03-12-12-10-09_db_56e3f58a06988.jpg)
####```www klasörüne giriyoruz. indirmiş olduğumuz dosyayı Apachenin kurulu olduğu dizine atıyoruz``` 
![Customer](http://indir.astald.com/dosyalar/screenshot-from-2016-03-12-12-10-36_db_56e3f58a0a25e.jpg)
####```composer install komutunu yazıyoruz``` 
####```kurulum başlıyor``` 
![Customer](http://indir.astald.com/dosyalar/screenshot-from-2016-03-12-12-10-42_db_56e3f58a0ea23.jpg)
![Customer](http://indir.astald.com/dosyalar/screenshot-from-2016-03-12-12-13-04_db_56e3f58a14951.jpg)
![Customer](http://indir.astald.com/dosyalar/screenshot-from-2016-03-12-12-14-14_db_56e3f58a18ab4.jpg)
####```kurulum başarıyla gerçekleşti``` 
![Customer](http://indir.astald.com/dosyalar/screenshot-from-2016-03-12-12-14-47_db_56e3f58a1e83a.jpg)
#####```.env klasörü gizli varsayılan olarak gizli olmaktadır. Gizli dosyaları görüntüledikten sonra .env dosyasına bağlanıp veritabanı bilgilerimizi gerçekleştirebiliriz``` 

# Önizleme
![Customer](http://indir.astald.com/dosyalar/screen_db_56cd781b15696.png)
![Customer](http://indir.astald.com/dosyalar/screen-kopya-3_db_56cd79dfe815a.png)
![Customer](http://indir.astald.com/dosyalar/screen-kopya-2_db_56cd79dfe7f86.png)
![Customer](http://indir.astald.com/dosyalar/screen-kopya-kopya_db_56cd79dfe7c6c.png)
![Customer](http://indir.astald.com/dosyalar/screen-kopya-kopya-kopya_db_56cd79dfde581.png)


# Destek için
http://blog.astald.com/musteri-takip-scripti/ kısmına yorum yazabilirsiniz.

# Bilgi
Anlayışınız için teşekkürler.

www.astald.com
